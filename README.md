# SPOTIFY APP ( MEAN )

## FRONTEND
Générer Access Token pour utiliser l'API Spotify depuis le lien https://accounts.spotify.com/authorize après avoir créer son compte sur spotify.com et renseigner les informations requises à l'application. Utiliser ID de l'appli + Secret Key fournies pour récupérer un code qui sert à générer la TOKEN avec la commande :

curl -H "Authorization: Basic MDdkZTdkNGE0NjRkNGM0NmIyN2YwNDk1ZmQzNjk0ZmM6YWZmNmM4ZTRlMzRlNDkxZWE5MGM4YjEwMWQ2NmJmZDI=" -d grant_type=authorization_code -d code= [CODE ICI] -d redirect_uri=[LIEN DE REDIRECTION ICI] https://accounts.spotify.com/api/token

La token générée est à placer dans le fichier ./assets/token.txt

L'accès à l'api vous permet de faire une recherche en temps réél des albums depuis le header de l'app


## BACKEND
Backend RestFUL avec le framework Express de NodeJS, connecté à MongoDB, cette API permet de switcher entre les utilisateurs, de reseigner sa propre bibliothèque et aussi de mettre en favoris des albums déjà intégrés dans la bibliothèque.


By Zahid
